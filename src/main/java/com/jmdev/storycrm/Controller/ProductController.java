package com.jmdev.storycrm.Controller;

import com.jmdev.storycrm.DTO.ProductDTO;
import com.jmdev.storycrm.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping ("/products")
    ResponseEntity<List<ProductDTO>> getAllProducts(){
        return new ResponseEntity<>(productService.findAll(),
                HttpStatus.OK);
    }

}
