package com.jmdev.storycrm.DTO;

import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class ProductDTO {

    private Long Id;

    private String division;

    private String productGroup;

    private String manufacturer;

    private String name;

    private String type;

    private String subType;

    private Map<String, String> specification;

    private Integer inStock;

    private Double purchasePrice;

    private Double sellPrice;

    private String slogan;

    private String description;

    private String mainPicture;

    private List<String> pictureGallery;

    private String mainVideo;

    private List<String> videoGallery;

    private String brochure;

    private Map<String, String> documents;

    private Double discountPercentageValue;


    public ProductDTO() {
    }

    public ProductDTO(String division, String productGroup, String manufacturer, String name, String type, String subType,
                      Map<String, String> specification, Integer inStock, Double purchasePrice, Double sellPrice, String slogan,
                      String description, String mainPicture, List<String> pictureGallery, String mainVideo,
                      List<String> videoGallery, String brochure, Map<String, String> documents, Double discountPercentageValue) {

        this.division = division;
        this.productGroup = productGroup;
        this.manufacturer = manufacturer;
        this.name = name;
        this.type = type;
        this.subType = subType;
        this.specification = specification;
        this.inStock = inStock;
        this.purchasePrice = purchasePrice;
        this.sellPrice = sellPrice;
        this.slogan = slogan;
        this.description = description;
        this.mainPicture = mainPicture;
        this.pictureGallery = pictureGallery;
        this.mainVideo = mainVideo;
        this.videoGallery = videoGallery;
        this.brochure = brochure;
        this.documents = documents;
        this.discountPercentageValue = discountPercentageValue;
    }
}
