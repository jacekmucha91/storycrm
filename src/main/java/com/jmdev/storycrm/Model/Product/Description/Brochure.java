package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@Entity
public class Brochure {

    @Id
    @GeneratedValue
    private Long Id;


    private String filePath;

    @OneToOne (mappedBy = "brochure")
    private Product product;


    public Brochure() {
    }

    public Brochure(String filePath) {
        this.filePath = filePath;
    }

    public Brochure(String filePath, Product product) {
        this.filePath = filePath;
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Brochure brochure = (Brochure) o;
        return Id.equals(brochure.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }

    @Override
    public String toString() {
        return "Brochure{" +
                "Id=" + Id +
                ", filePath='" + filePath + '\'' +
                ", product=" + product +
                '}';
    }
}
