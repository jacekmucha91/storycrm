package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
public class Division {

    @Id
    @GeneratedValue
    private Long Id;

    private String name;

    @OneToMany (mappedBy = "division")
    private List<Product> products;


    public Division() {
    }

    public Division(String name) {
        this.name = name;
    }

    public Division(String name, List<Product> products) {
        this.name = name;
        this.products = products;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Division that = (Division) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }




    @Override
    public String toString() {
        return "Division{" +
                "Id=" + Id +
                ", filePath='" + name + '\'' +
                ", products=" + products +
                '}';
    }
}
