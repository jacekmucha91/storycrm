package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Objects;

@Getter
@Setter
@Entity
public class MainPicture {

    @Id
    @GeneratedValue
    private Long id;

    private String filePath;

    @OneToOne (mappedBy = "mainPicture")
    private Product product;

    public MainPicture() {
    }

    public MainPicture(String filePath) {
        this.filePath = filePath;
    }

    public MainPicture(String filePath, Product product) {
        this.filePath = filePath;
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MainPicture that = (MainPicture) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "MainPicture{" +
                "id=" + id +
                ", filePath='" + filePath + '\'' +
                ", product=" + product +
                '}';
    }
}

