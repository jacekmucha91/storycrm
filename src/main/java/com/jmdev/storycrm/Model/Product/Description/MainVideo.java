package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Objects;

@Getter
@Setter
@Entity
public class MainVideo {

    @Id
    @GeneratedValue
    private Long id;

    private String filePath;

    @OneToOne(mappedBy = "mainVideo")
    private Product product;


    public MainVideo() {
    }

    public MainVideo(String filePath) {
        this.filePath = filePath;
    }

    public MainVideo(String filePath, Product product) {
        this.filePath = filePath;
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MainVideo mainVideo = (MainVideo) o;
        return id.equals(mainVideo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "MainVideo{" +
                "id=" + id +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}
