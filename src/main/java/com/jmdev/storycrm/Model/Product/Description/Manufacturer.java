package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
public class Manufacturer {

    @Id
    @GeneratedValue
    private Long Id;

    private String name;

    @OneToMany (mappedBy = "manufacturer")
    private List<Product> products;


    public Manufacturer() {
    }

    public Manufacturer(String name, List<Product> products) {
        this.name = name;
        this.products = products;
    }

    public Manufacturer(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manufacturer that = (Manufacturer) o;
        return Id.equals(that.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }

    @Override
    public String toString() {
        return "Manufacturer{" +
                "Id=" + Id +
                ", filePath='" + name + '\'' +
                ", products=" + products +
                '}';
    }
}
