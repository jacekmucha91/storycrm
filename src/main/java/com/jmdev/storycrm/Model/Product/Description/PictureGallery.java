package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
public class PictureGallery {

    @Id
    @GeneratedValue
    private Long Id;

    @Column
    @ElementCollection (targetClass = String.class)
    private List<String> filePaths;

    @OneToOne (mappedBy = "pictureGallery")
    private Product product;


    public PictureGallery() {
    }

    public PictureGallery(List<String> filePaths) {
        this.filePaths = filePaths;
    }

    public PictureGallery(List<String> filePaths, Product product) {
        this.filePaths = filePaths;
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PictureGallery that = (PictureGallery) o;
        return Id.equals(that.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }

    @Override
    public String toString() {
        return "PictureGallery{" +
                "Id=" + Id +
                ", filePaths=" + filePaths +
                ", product=" + product +
                '}';
    }
}
