package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Map;
import java.util.Objects;

@Getter
@Setter
@Entity
public class ProductDocuments {

    @Id
    @GeneratedValue
    private Long Id;

    @ElementCollection
    private Map<String, String> namesAndFilePaths;

    @OneToOne (mappedBy = "documents")
    private Product product;


    public ProductDocuments() {
    }

    public ProductDocuments(Map<String, String> namesAndFilePaths) {
        this.namesAndFilePaths = namesAndFilePaths;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDocuments that = (ProductDocuments) o;
        return Id.equals(that.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }

    @Override
    public String toString() {
        return "ProductDocuments{" +
                "Id=" + Id +
                ", namesAndFilePaths=" + namesAndFilePaths +
                ", product=" + product +
                '}';
    }
}
