package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
public class ProductGroup {

    @Id
    @GeneratedValue
    private Long Id;

    private String name;

    @OneToMany (mappedBy = "productGroup")
    private List<Product> products;


    public ProductGroup() {
    }

    public ProductGroup(String name) {
        this.name = name;
    }

    public ProductGroup(String name, List<Product> products) {
        this.name = name;
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductGroup productGroup = (ProductGroup) o;
        return Objects.equals(Id, productGroup.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }

    @Override
    public String toString() {
        return "ProductGroup{" +
                "Id=" + Id +
                ", filePath='" + name + '\'' +
                ", products=" + products +
                '}';
    }
}
