package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Getter
@Setter
@Entity
public class Specification {

    @Id
    @GeneratedValue
    private Long Id;

    @ElementCollection
    private Map<String, String> parameterAndValues;

    @OneToMany(mappedBy = "specification")
    private List<Product> products;


    public Specification() {
    }

    public Specification(Map<String, String> parameterAndValues) {
        this.parameterAndValues = parameterAndValues;
    }

    public Specification(Map<String, String> parameterAndValues, List<Product> products) {
        this.parameterAndValues = parameterAndValues;
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Specification that = (Specification) o;
        return Id.equals(that.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }

    @Override
    public String toString() {
        return "Specification{" +
                "Id=" + Id +
                ", parameterAndValues=" + parameterAndValues +
                ", products=" + products +
                '}';
    }
}
