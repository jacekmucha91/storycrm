package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
public class SubType {

    @Id
    @GeneratedValue
    private Long Id;

    private String name;

    @OneToMany (mappedBy = "subType")
    private List<Product> products;


    public SubType() {
    }

    public SubType(String name) {
        this.name = name;
    }

    public SubType(String name, List<Product> products) {
        this.name = name;
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubType subType = (SubType) o;
        return Id.equals(subType.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }


    @Override
    public String toString() {
        return "SubType{" +
                "Id=" + Id +
                ", filePath='" + name + '\'' +
                ", products=" + products +
                '}';
    }
}

