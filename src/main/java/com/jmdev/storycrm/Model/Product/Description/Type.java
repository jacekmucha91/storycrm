package com.jmdev.storycrm.Model.Product.Description;

import com.jmdev.storycrm.Model.Product.Product;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
public class Type {

    @Id
    @GeneratedValue
    private Long Id;

    private String name;

    @OneToMany (mappedBy = "type")
    private List<Product> products;


    public Type() {
    }

    public Type(String name) {
        this.name = name;
    }

    public Type(String name, List<Product> products) {
        this.name = name;
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Type type = (Type) o;
        return Id.equals(type.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }

    @Override
    public String toString() {
        return "Type{" +
                "Id=" + Id +
                ", filePath='" + name + '\'' +
                ", products=" + products +
                '}';
    }
}
