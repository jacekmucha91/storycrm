package com.jmdev.storycrm.Model.Product;

import com.jmdev.storycrm.Model.Product.Description.*;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long Id;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "division_id")
    private Division division;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "group_id")
    private ProductGroup productGroup;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "manufacturer_id")
    private Manufacturer manufacturer;

    private String name;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "type_id")
    private Type type;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "subType_id")
    private SubType subType;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "specification_id")
    private Specification specification;

    private Integer inStock;

    private Double purchasePrice;

    private Double sellPrice;

    private String slogan;

    private String description;

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "main_picture_id")
    private MainPicture mainPicture;

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "picture_gallery_id")
    private PictureGallery pictureGallery;

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "main_video_id")
    private MainVideo mainVideo;

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "video_gallery_id")
    private VideoGallery videoGallery;

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "brochure_id")
    private Brochure brochure;

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "documents_id")
    private ProductDocuments documents;

    private Double discountPercentageValue;


    public Product() {
    }

    public Product(Division division, ProductGroup productGroup, Manufacturer manufacturer, String name, Type type,
                   SubType subType, Specification specification, Integer inStock, Double purchasePrice,
                   Double sellPrice, String slogan, String description, MainPicture mainPicture,
                   PictureGallery pictureGallery, MainVideo mainVideo, VideoGallery videoGallery,
                   Brochure brochure, ProductDocuments documents, Double discountPercentageValue) {
        this.division = division;
        this.productGroup = productGroup;
        this.manufacturer = manufacturer;
        this.name = name;
        this.type = type;
        this.subType = subType;
        this.specification = specification;
        this.inStock = inStock;
        this.purchasePrice = purchasePrice;
        this.sellPrice = sellPrice;
        this.slogan = slogan;
        this.description = description;
        this.mainPicture = mainPicture;
        this.pictureGallery = pictureGallery;
        this.mainVideo = mainVideo;
        this.videoGallery = videoGallery;
        this.brochure = brochure;
        this.documents = documents;
        this.discountPercentageValue = discountPercentageValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Id.equals(product.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }

    @Override
    public String toString() {
        return "Product{" +
                "Id=" + Id +
                ", division=" + division +
                ", productGroup=" + productGroup +
                ", manufacturer=" + manufacturer +
                ", filePath='" + name + '\'' +
                ", type=" + type +
                ", subType=" + subType +
                ", parameterAndValues=" + specification +
                ", inStock=" + inStock +
                ", purchasePrice=" + purchasePrice +
                ", sellPrice=" + sellPrice +
                ", slogan='" + slogan + '\'' +
                ", description='" + description + '\'' +
                ", mainPicture=" + mainPicture +
                ", pictureGallery=" + pictureGallery +
                ", mainVideo=" + mainVideo +
                ", videoGallery=" + videoGallery +
                ", brochure=" + brochure +
                ", documents=" + documents +
                ", discountPercentageValue=" + discountPercentageValue +
                '}';
    }



}
