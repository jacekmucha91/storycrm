package com.jmdev.storycrm.Service;

import com.jmdev.storycrm.DTO.ProductDTO;
import com.jmdev.storycrm.Repository.ProductRepository;
import com.jmdev.storycrm.Utilities.Assembler.ProductAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;


    public List<ProductDTO> findAll() {

        ProductAssembler productAssembler = null;
        List<ProductDTO> dtoList = productRepository.findAll().stream()
                .map(product -> ProductAssembler.getAssembler().convertToDTO(product))
                .collect(Collectors.toList());

        return dtoList;
    }
}
