package com.jmdev.storycrm;

import com.jmdev.storycrm.DTO.ProductDTO;
import com.jmdev.storycrm.Model.Product.Product;
import com.jmdev.storycrm.Repository.ProductRepository;
import com.jmdev.storycrm.Utilities.Assembler.ProductAssembler;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class StorycrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(StorycrmApplication.class, args);
	}

	@Bean
	public CommandLineRunner setup(ProductRepository productRepository) {
		return (args) -> {

			Map<String, String> specification = new HashMap<>();
			specification.put("Quality","High");
			specification.put("Accuracy","1 cm");
			specification.put("Ruggedness","2 m drop");

			List<String> picturesGallery = new ArrayList<>();
			picturesGallery.add("picture-link1");
			picturesGallery.add("picture-link2");
			picturesGallery.add("picture-link3");

			List<String> videosGallery = new ArrayList<>();
			videosGallery.add("video-link1");
			videosGallery.add("video-link2");
			videosGallery.add("video-link3");

			Map<String, String> documents = new HashMap<>();
			documents.put("whitePaper","doc1.pdf");
			documents.put("Second WhitePaper","doc2.pdf");


			ProductDTO dtoSample1 = new ProductDTO(
					"UAV","Drones","DJI","Matrice 210","Full","Full", specification,
					50,9000D,16000D,"Sky is the limit!", "very long and well prepared description",
					"resource path for picture", picturesGallery, "resource path for video", videosGallery, "brochure link",
					documents, 0.07D
			);
			ProductDTO dtoSample2 = new ProductDTO(
					"Landuse","Total Stations","Spectra","F35","Robotic","1 sec", specification,
					20,17000D,30000D,"Total Station!", "very long and well prepared description",
					"resource path for picture", picturesGallery, "resource path for video", videosGallery, "brochure link",
					documents, 0.03D
			);

			productRepository.save(ProductAssembler.getAssembler().convertToEntity(dtoSample1));
			productRepository.save(ProductAssembler.getAssembler().convertToEntity(dtoSample2));

		};
	}

}
