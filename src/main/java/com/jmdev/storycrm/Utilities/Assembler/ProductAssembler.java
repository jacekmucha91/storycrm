package com.jmdev.storycrm.Utilities.Assembler;

import com.jmdev.storycrm.DTO.ProductDTO;
import com.jmdev.storycrm.Model.Product.Description.*;
import com.jmdev.storycrm.Model.Product.Product;
import com.jmdev.storycrm.Utilities.ModelMapper.ModelMapperSingleton;

public class ProductAssembler {


    private static ProductAssembler PRODUCT_ASSEMBLER;

    public static ProductAssembler getAssembler(){
        if (PRODUCT_ASSEMBLER == null){
            PRODUCT_ASSEMBLER = new ProductAssembler();
        }
        return PRODUCT_ASSEMBLER;
    }

    public Product convertToEntity(ProductDTO productDTO) {

        Product product = ModelMapperSingleton.getMapper().map(productDTO, Product.class);
        product.setDivision(new Division(productDTO.getDivision()));
        product.setProductGroup(new ProductGroup(productDTO.getProductGroup()));
        product.setManufacturer(new Manufacturer(productDTO.getManufacturer()));
        product.setName(productDTO.getName());
        product.setType(new Type(productDTO.getType()));
        product.setSubType(new SubType(productDTO.getSubType()));
        product.setSpecification(new Specification(productDTO.getSpecification()));
        product.setInStock(productDTO.getInStock());
        product.setPurchasePrice(productDTO.getPurchasePrice());
        product.setSellPrice(productDTO.getSellPrice());
        product.setSlogan(productDTO.getSlogan());
        product.setDescription(productDTO.getDescription());
        product.setMainPicture(new MainPicture(productDTO.getMainPicture()));
        product.setPictureGallery(new PictureGallery(productDTO.getPictureGallery()));
        product.setMainVideo(new MainVideo(productDTO.getMainVideo()));
        product.setVideoGallery(new VideoGallery(productDTO.getVideoGallery()));
        product.setBrochure(new Brochure(productDTO.getBrochure()));
        product.setDocuments(new ProductDocuments(productDTO.getDocuments()));
        product.setDiscountPercentageValue(productDTO.getDiscountPercentageValue());

        return product;
    }

    public ProductDTO convertToDTO(Product product){

        ProductDTO dto = new ProductDTO();
        dto.setDivision(product.getDivision().getName());
        dto.setProductGroup(product.getProductGroup().getName());
        dto.setManufacturer(product.getManufacturer().getName());
        dto.setName(product.getName());
        dto.setType(product.getType().getName());
        dto.setSubType(product.getSubType().getName());
        dto.setSpecification(product.getSpecification().getParameterAndValues());
        dto.setInStock(product.getInStock());
        dto.setPurchasePrice(product.getPurchasePrice());
        dto.setSellPrice(product.getSellPrice());
        dto.setSlogan(product.getSlogan());
        dto.setDescription(product.getDescription());
        dto.setMainPicture(product.getMainPicture().getFilePath());
        dto.setPictureGallery(product.getPictureGallery().getFilePaths());
        dto.setMainVideo(product.getMainVideo().getFilePath());
        dto.setVideoGallery(product.getVideoGallery().getFilePaths());
        dto.setBrochure(product.getBrochure().getFilePath());
        dto.setDocuments(product.getDocuments().getNamesAndFilePaths());
        dto.setDiscountPercentageValue(product.getDiscountPercentageValue());

        return dto;
    }


}
