package com.jmdev.storycrm.Controller;

import com.jmdev.storycrm.DTO.ProductDTO;
import com.jmdev.storycrm.Service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import java.util.*;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@WebMvcTest
public class ProductControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Test
    void getAllProducts() throws Exception {
        List<ProductDTO> productsList = new ArrayList<>();

        Map<String, String> specification = new HashMap<>();
        specification.put("Quality","High");
        specification.put("Accuracy","1 cm");
        specification.put("Ruggedness","2 m drop");

        Map<String, String> documents = new HashMap<>();
        documents.put("whitePaper","doc1.pdf");
        documents.put("Second WhitePaper","doc2.pdf");

        List<String> picturesGallery = new ArrayList<>();
        picturesGallery.add("picture-link1");
        picturesGallery.add("picture-link2");
        picturesGallery.add("picture-link3");

        List<String> videosGallery = new ArrayList<>();
        videosGallery.add("video-link1");
        videosGallery.add("video-link2");
        videosGallery.add("video-link3");



        productsList.add(new ProductDTO(
                "Landuse","GNSS","Spectra","SP60","Full","Full", specification,
                100,6000D,12000D,"Great equipment!", "very long and well prepared description",
                "resource path for picture", picturesGallery, "resource path for video", videosGallery, "brochure link",
                documents, 0.05D
        ));

        productsList.add(new ProductDTO(
                "Landuse","Total Stations","Spectra","F35","Robotic","1 sec", specification,
                20,17000D,30000D,"Total Station!", "very long and well prepared description",
                "resource path for picture", picturesGallery, "resource path for video", videosGallery, "brochure link",
                documents, 0.03D
        ));

        productsList.add(new ProductDTO(
                "UAV","Drones","DJI","Matrice 210","Full","Full", specification,
                50,9000D,16000D,"Sky is the limit!", "very long and well prepared description",
                "resource path for picture", picturesGallery, "resource path for video", videosGallery, "brochure link",
                documents, 0.07D
        ));

        when(productService.findAll()).thenReturn(productsList);

        mockMvc.perform(MockMvcRequestBuilders.get("/products")
        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(jsonPath("$",hasSize(3))).andDo(print());

    }

}
