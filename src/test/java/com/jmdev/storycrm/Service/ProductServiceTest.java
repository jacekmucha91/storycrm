package com.jmdev.storycrm.Service;

import com.jmdev.storycrm.Utilities.Assembler.ProductAssembler;
import com.jmdev.storycrm.DTO.ProductDTO;
import com.jmdev.storycrm.Repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    void getAllProducts(){

        ProductAssembler productAssembler = new ProductAssembler();

        Map<String, String> specification = new HashMap<>();
        specification.put("Quality","High");
        specification.put("Accuracy","1 cm");
        specification.put("Ruggedness","2 m drop");

        List<String> picturesGallery = new ArrayList<>();
        picturesGallery.add("picture-link1");
        picturesGallery.add("picture-link2");
        picturesGallery.add("picture-link3");

        List<String> videosGallery = new ArrayList<>();
        videosGallery.add("video-link1");
        videosGallery.add("video-link2");
        videosGallery.add("video-link3");

        Map<String, String> documents = new HashMap<>();
        documents.put("whitePaper","doc1.pdf");
        documents.put("Second WhitePaper","doc2.pdf");

        ProductDTO sampleToSave1 = new ProductDTO(
                "Landuse","GNSS","Spectra","SP60","Full","Full", specification,
                100,6000D,12000D,"Great equipment!", "very long and well prepared description",
                "resource path for picture", picturesGallery, "resource path for video", videosGallery, "brochure link",
                documents, 0.05D
        );
        ProductDTO sampleProduct2 = new ProductDTO(
                "Landuse","Total Stations","Spectra","F35","Robotic","1 sec", specification,
                20,17000D,30000D,"Total Station!", "very long and well prepared description",
                "resource path for picture", picturesGallery, "resource path for video", videosGallery, "brochure link",
                documents, 0.03D
        );

        productRepository.save(ProductAssembler.getAssembler().convertToEntity(sampleToSave1));
        ProductDTO sampleLoaded = ProductAssembler.getAssembler().convertToDTO(productRepository.findAll().get(0));
        
        assertEquals(sampleLoaded.getDivision(), sampleToSave1.getDivision());
    }

}
